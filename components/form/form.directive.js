(function(){
    "use strict";

    angular
        .module('ngTodo')
        .directive('todoForm', function() {
            return {
                templateUrl: 'components/form/form.component.html'
            }
        });
})();
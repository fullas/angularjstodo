(function(){
    
    angular
        .module('ngTodo')
        .controller('formCtrl', ['$scope', 'todoFactory', function($scope, todoFactory){
            
            $scope.todos = todoFactory.getArray();
            $scope.newTodo = {};
            $scope.todoName = '';
            $scope.todoView = 'all';

            $scope.$on('refresh', function($event, data){
                $scope.todoView = data.b;
            });

            $scope.isInvalid = function() {
                return $scope.todoName.length === 0 || !/^.*[^\s]+.*$/.test($scope.todoName)
            }

            $scope.submit = function() {
                if ($scope.isInvalid()) {
                    return;
                }

                $scope.newTodo.id = Date.now();
                $scope.newTodo.name = $scope.todoName;
                $scope.newTodo.date = new Date();
                $scope.newTodo.checked = false;
                
                todoFactory.add($scope.newTodo);
                $scope.todos = todoFactory.showTodos($scope.todoView);
                $scope.$emit('toRefresh', {a: $scope.todos, b:$scope.todoView});

                $scope.todoName = '';
                $scope.newTodo = {};
            }
        }]);
})();
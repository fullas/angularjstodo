(function(){
    "use strict";

    angular
        .module('ngTodo')
        .factory('todoFactory', ['baseFactory', function(baseFactory){
            var todoView = 'all';

            function getObj() {
                return baseFactory.get('todos');
            }

            function getArray() {
                var items = getObj();
                var array = Object.keys(items).map(id => items[id]);
                return array || [];
            }

            function getInfo() {
                var info = [];
                var all = getArray();
                var complete = all.filter(todo => todo.checked).length;
                var remained = all.length - complete;
                info.push(all.length, complete, remained);
                return info || [];
            }

            function add(todo) {
                var todos = getObj();

                todos[todo.id] = todo;
                
                baseFactory.set('todos', todos);
                getArray();
            }

            function updateTodo(id) {
                var todos = getObj();

                todos[id].checked = !todos[id].checked;

                baseFactory.set('todos', todos);
                getArray();
            }

            function deleteTodo(id) {
                var todos = getObj();

                delete todos[id];

                baseFactory.set('todos', todos);
                getArray();
            }

            function deleteAll() {
                todoView = 'all';
                baseFactory.set('todos', {});
                getArray();
                showTodos(todoView);
            }
            
            function getAll() {
                todoView = 'all';
                var all = getArray();
                return all;
            }

            function getCompleted() {
                todoView = 'completed';
                var completed = getArray().filter(todo => todo.checked);
                return completed;
            }

            function getRemaining() {
                todoView = 'remaining';
                var remaining = getArray().filter(todo => !todo.checked);
                return remaining;
            }

            function showTodos(view) {
                switch (view) {
                    case 'all': {
                        var todos = getArray();
                        getInfo();
                        return todos;
                        break;
                    }
                    case 'completed': {
                        var completed = getCompleted();
                        if (completed.length === 0) {
                            todoView = 'all';
                            showTodos(todoView);
                        }
                        getInfo();
                        return completed;
                        break;
                    }
                    case 'remaining': {
                        var remaining = getRemaining();
                        if (remaining.length === 0) {
                            todoView = 'all';
                            showTodos(todoView);
                        }
                        getInfo();
                        return remaining;
                        break;
                    }
                    default: {
                        var todos = getArray();
                        getInfo();
                        return todos || [];
                        break;
                    }
                }
            }

            return {
                add: add,
                deleteAll: deleteAll,
                deleteTodo: deleteTodo,
                getAll: getAll,
                getArray: getArray,
                getCompleted: getCompleted,
                getInfo: getInfo,
                getRemaining: getRemaining,
                showTodos: showTodos,
                updateTodo: updateTodo
            }
        }]);
})();
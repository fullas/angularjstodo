(function(){
    "use strict";

    angular
        .module('ngTodo')
        .factory('baseFactory', [function(){
            
            var appKey = 'myTodos';

            function get(key) {
                var data = JSON.parse(localStorage.getItem(appKey)) || {};

                return data[key] || {};
            }

            function set(key, value) {
                var data = JSON.parse(localStorage.getItem(appKey)) || {};

                data[key] = value; 
                
                localStorage.setItem(appKey, JSON.stringify(data));
            }

            return {
                get: get,
                set: set
            }
        }])
})();
(function(){
    "use strict";

    angular
        .module('ngTodo')
        .directive('todoFooter', function(){
            return {
                templateUrl: 'components/footer/footer.component.html'
            }
        });
})();
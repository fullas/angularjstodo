(function(){
    
    angular
        .module('ngTodo')
        .controller('footerCtrl', ['$scope', 'todoFactory', function($scope, todoFactory){
            $scope.todoView = 'all';
            $scope.isOpen = false;
            $scope.info = todoFactory.getInfo();

            $scope.todos = todoFactory.getArray();

            $scope.$on('refresh', function($event, data){
                $scope.todos = data.a;
                $scope.todoView = data.b;
                $scope.info = data.c;
                todoFactory.todoView = data.b;
            });

            $scope.onRemoveAll = function() {
                $scope.todoView = 'all';
                todoFactory.deleteAll();
                $scope.todos = todoFactory.getArray();
                appRefresh();
            }

            $scope.onListAll = function() {
                $scope.todoView = 'all';
                $scope.todos = todoFactory.getArray();
                appRefresh();
            }

            $scope.onListCompleted = function() {
                $scope.todoView = 'completed';
                $scope.todos = todoFactory.getCompleted();
                appRefresh();
            }

            $scope.onListRemaining = function() {
                $scope.todoView = 'remaining';
                $scope.todos = todoFactory.getRemaining();
                appRefresh();
            }

            function appRefresh() {
                $scope.isOpen = false;
                $scope.info = todoFactory.getInfo();
                todoFactory.showTodos($scope.todoView);
                $scope.$emit('toRefresh', {a: $scope.todos, b: $scope.todoView});
            }
        }]);
})();
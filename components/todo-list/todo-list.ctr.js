(function(){
    
    angular
        .module('ngTodo')
        .controller('todoListCtrl', ['$scope', 'todoFactory', function($scope, todoFactory){
            
            $scope.todos = todoFactory.getArray().reverse();
            
            $scope.$on('refresh', function($event, data){
                $scope.todos = data.a.reverse();
                $scope.todoView = data.b;
            });

            $scope.onDelete = function(todo) {
                todoFactory.deleteTodo(todo.id);
                resetView();
            }

            $scope.onCheck = function(todo) {
                todoFactory.updateTodo(todo.id);
                resetView();
            }

            function resetView() {
                $scope.todos = todoFactory.showTodos($scope.todoView);
                if (!$scope.todos.length) {
                    $scope.todoView = 'all';
                }
                $scope.todos = todoFactory.showTodos($scope.todoView);
                $scope.$emit('toRefresh', {a: $scope.todos, b: $scope.todoView});
            }
        }]);
})();
(function(){
    "use strict";

    angular
        .module('ngTodo')
        .directive('todoList', function(){
            return {
                templateUrl: 'components/todo-list/todo-list.component.html'
            }
        });
})();
(function() {

    angular
        .module('ngTodo')
        .controller('todosCtrl', ['$scope', '$interval', 'todoFactory', function($scope, $interval, todoFactory) {
            $scope.todos = todoFactory.getArray();
            $scope.todoView = 'all';
            $scope.date = new Date();
            
            $interval(function() {
                $scope.date = new Date();
            }, 1000);

            $scope.$on('toRefresh', function($event, data){
                $scope.todos = data.a;
                $scope.todoView = data.b;
                $scope.$broadcast('refresh', {a: $scope.todos, b: $scope.todoView, c: todoFactory.getInfo()});
            });
        }]);
})();
(function() {
    "use strict";

    angular
        .module('ngTodo')
        .directive('todoHeader', function() {
            return {
                templateUrl: 'components/header/header.component.html'
            }
        })
})();